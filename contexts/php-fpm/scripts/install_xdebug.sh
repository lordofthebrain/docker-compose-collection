#!/bin/bash

set -eu

PHP_VERSION_NUMBER=$(echo "${PHP_VERSION}" | sed -E 's|^([0-9]+)\.([0-9]).*|\1\2|')

if [[ "${PHP_VERSION_NUMBER}" -lt "72" ]]; then
    echo "xdebug requires PHP version >= 7.2.0, installed version is ${PHP_VERSION}"
    exit 1;
fi

VERSION=
if [[ "${PHP_VERSION_NUMBER}" -lt "80" ]]; then
    # use last 3.1.x version
    VERSION=-$(git ls-remote --tags --sort=refname https://github.com/xdebug/xdebug | awk -F/ '/refs\/tags\/[3].[1].[0-9]*[^}]$/{print $NF}' | tail -1)
fi

pecl install xdebug${VERSION}
docker-php-ext-enable xdebug
echo 'xdebug.mode=develop,debug' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
echo 'xdebug.start_with_request=yes' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
echo 'xdebug.discover_client_host=0' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
echo 'xdebug.client_host=host.docker.internal' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
echo 'xdebug.log_level=0' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini