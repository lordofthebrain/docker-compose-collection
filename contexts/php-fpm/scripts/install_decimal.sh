#!/bin/bash

set -eu

MPDEC_RELEASE_NAME="mpdecimal-2.5.1"
MPDEC_URL="https://www.bytereef.org/software/mpdecimal/releases/${MPDEC_RELEASE_NAME}.tar.gz"
MPDEC_SHA256_SUM="9f9cd4c041f99b5c49ffb7b59d9f12d95b683d88585608aa56a6307667b2b21f"
MPDEC_TMP_DIR="/tmp/${MPDEC_RELEASE_NAME}"
MPDEC_TMP_ARCHIVE="${MPDEC_TMP_DIR}/${MPDEC_RELEASE_NAME}.tar.gz"

mkdir -p "${MPDEC_TMP_DIR}"
cd "${MPDEC_TMP_DIR}"
curl --progress-bar -LO "${MPDEC_URL}"
echo "${MPDEC_SHA256_SUM} ${MPDEC_TMP_ARCHIVE}" | sha256sum --check --status -
tar xf "${MPDEC_TMP_ARCHIVE}"
cd "${MPDEC_RELEASE_NAME}"
./configure && make && make install
rm -rf "${MPDEC_TMP_DIR}"
pecl install decimal && docker-php-ext-enable decimal

