#!/bin/bash

set -eu

apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libwebp-dev

if [[ $(echo "${PHP_VERSION}" | sed -E 's|^([0-9]+)\.([0-9]).*|\1\2|') -lt "74" ]]
then
    docker-php-ext-configure gd \
        --with-freetype-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/
else
    docker-php-ext-configure gd \
        --enable-gd \
        --with-freetype \
        --with-jpeg \
        --with-webp
fi

docker-php-ext-install -j$(nproc) iconv
docker-php-ext-install -j "$(nproc)" gd
