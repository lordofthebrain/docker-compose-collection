#!/usr/bin/env bash
DIR="$(dirname ${0})/../../"

cd ${DIR}

if [[ ! -f ".env" ]]; then
    if [[ -f .env.example ]]; then
      cp .env.example .env
    else
      touch .env
    fi
fi

TIMEZONE=$(grep -r TIMEZONE .env | cut -d= -f2)
TIMEZONE=${TIMEZONE:-Europe/Berlin}
ls -l "/usr/share/zoneinfo/${TIMEZONE}" >/dev/null
if [[ $? -ne 0 ]]; then
    printf "ERROR: the timezone you specified could not be found\n"
    exit 1
fi

USER_ID=$(grep -r UID .env | cut -d= -f2)
USER_ID=${USER_ID:-1000}
if [[ ${USER_ID} -eq 0 ]]; then
    printf "ERROR: the user id must not be 0 (root user id)\n"
    exit 1
fi

if [[ ! -f ./bin/external/yq ]]; then
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS
        curl -sL https://github.com/mikefarah/yq/releases/download/v4.24.5/yq_darwin_amd64 -o ./bin/external/yq
    else
        # linux
        curl -sL https://github.com/mikefarah/yq/releases/download/v4.24.5/yq_linux_amd64 -o ./bin/external/yq
    fi

    chmod +x ./bin/external/yq
fi
