set -e

check() {
    SERVICES=${1}
    CONFIG_KEY=${2}
    SERVICE_NAME=${3}

    if [[ ! $(grep -r ${CONFIG_KEY} .env | cut -d= -f2) == "true" ]]; then
        SERVICES=$(echo -n ${SERVICES} | sed -E "s/(^|[[:space:]])${SERVICE_NAME}([[:space:]]|$)/ /")
    fi

    echo -n ${SERVICES}
}

if [[ -z ${1} ]]; then
    SERVICES=$(echo $(./bin/external/yq '.services | keys' docker-compose.yml | cut -d' ' -f2))

    SERVICES=$(check "${SERVICES}" "DEFAULT_START_PHPMYADMIN" "phpmyadmin")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_NGINX" "nginx")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_APACHE" "apache")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_MYSQL" "mysql")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_REDIS" "redis")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_PHPCACHEADMIN" "phpcacheadmin")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_MAILHOG" "mailhog")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_SELENIUM_CHROME" "selenium-chrome")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_SELENIUM_FIREFOX" "selenium-firefox")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_HTTPBIN" "httpbin")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_TEST_PICTURE_PROVIDER" "test-picture-provider")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_CHROME" "chrome")
else
    SERVICES=${@}
fi
