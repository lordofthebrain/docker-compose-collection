#!/usr/bin/env bash
DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME:-my_fancy_app}
test -t 1 && USE_TTY="-t" || USE_TTY=
docker exec -i ${USE_TTY} --user=www-data "${COMPOSE_PROJECT_NAME}_php-fpm" /bin/bash "${@}"
cd - > /dev/null
