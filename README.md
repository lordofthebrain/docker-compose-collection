[![Latest Release](https://gitlab.com/lordofthebrain/docker-compose-collection/-/badges/release.svg)](https://gitlab.com/lordofthebrain/docker-compose-collection/-/releases) [![pipeline status](https://gitlab.com/lordofthebrain/docker-compose-collection/badges/master/pipeline.svg)](https://gitlab.com/lordofthebrain/docker-compose-collection/-/commits/master)
# docker-compose-collection

**versions before 5.0.0 can be found under:**  https://gitlab.com/bosi/docker-compose-collection

Inspired by [laradock](https://laradock.io) this project contains a useful docker-compose.yml file for working with php, codeception, laravel, .... I preferred creating a collection by myself because there were several things I couldn't do with laradock.

## Requirement
* **docker compose >= 2.25.05**
# Usage
## Via docker-compose
To use this project you just have to do 3 steps:
1. clone it
2. copy .env.example to .env (or use ```./bin/init.sh```)
3. run ```./bin/start.sh```

## Via docker swarm
1. clone it
2. copy .env.example to .env
3. join or create a docker swarm (see [https://docs.docker.com/engine/swarm/](https://docs.docker.com/engine/swarm/) for more details)
4. run ```docker stack deploy -c <(./bin/config.sh) my_fancy_app```

