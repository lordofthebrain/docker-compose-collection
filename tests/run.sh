#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

PROJECT_PATH="$(dirname ${0})/.."
cd ${PROJECT_PATH}

./tests/bats-core/bin/bats tests/tests/ -t -T -f "$*"
EXIT_CODE=$?
cd - > /dev/null

printf "\n Tests runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"

exit ${EXIT_CODE}
