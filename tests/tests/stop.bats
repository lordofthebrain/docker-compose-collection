#!/usr/bin/env bats

load helpers

run_script () {
    run bin/stop.sh
}

run_cli_script () {
    run bin/dcc stop
}

@test $(get_description "check if script runs when .env is missing") {
    check_env_test
}

