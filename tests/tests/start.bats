#!/usr/bin/env bats

load helpers

run_script () {
    run bin/start.sh
}

run_cli_script () {
    run bin/dcc start
}

@test $(get_description "check if script runs when .env is missing") {
    check_env_test
}

@test $(get_description "check if services to be started are determined correctly") {
    . ./bin/subscripts/services.sh
    [ "${SERVICES}" == "php-fpm nginx mysql" ]
    sed -i "s|^\(DEFAULT_START_SELENIUM_CHROME=\s*\).*$|\1true|" .env
    . ./bin/subscripts/services.sh
    [ "${SERVICES}" == "php-fpm nginx mysql selenium-chrome" ]
    RESULT=$(check "nginx 2chrome chrome3 chrome" "DEFAULT_START_CHROME" "chrome")
    [ "${RESULT}" == "nginx 2chrome chrome3" ]
    RESULT=$(check "chrome 2chrome chrome3" "DEFAULT_START_CHROME" "chrome")
    [ "${RESULT}" == "2chrome chrome3" ]
}

