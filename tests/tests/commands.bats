#!/usr/bin/env bats

load helpers

@test $(get_description "check mariadb, mariadb-dump, mysql and mysql-dump commands") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    sed -i "s|^\(NPM=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'mariadb --version'
    [ $(echo $output | grep -i "mariadb from" | wc -l) -eq 1 ]
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'mariadb-dump --version'
    [ $(echo $output | grep -i "mariadb-dump from" | wc -l) -eq 1 ]
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'mysql --version'
    [ $(echo $output | grep -i "mysql from" | wc -l) -eq 1 ]
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'mysql-dump --version'
    [ $(echo $output | grep -i "mysql-dump from" | wc -l) -eq 1 ]
}

