#!/usr/bin/env bats

load helpers

@test $(get_description "check if composer cache is working") {
    sed -i "s|^\(PHP_EXT_PDO_MYSQL=\s*\).*$|\1false|" .env
    ./bin/start.sh php-fpm
    ./bin/exec.sh rm -rf ~/.composer
    ./bin/exec.sh composer init -n --name=test/test
    run ./bin/exec.sh composer require rivsen/hello-world
    [ $(echo $output | grep -i "Downloading" | wc -l) -eq 1 ]
    ./bin/exec.sh rm -r vendor/
    run ./bin/exec.sh composer install
    [ $(echo $output | grep -i "Downloading" | wc -l) -eq 0 ]
}

@test $(get_description "check if cron is running") {
    ./bin/start.sh php-fpm
    run ./bin/exec.sh service cron status
    [ $(echo $output | grep -i "cron is running" | wc -l) -eq 1 ]
}

@test $(get_description "check if vite port is published") {
    ./bin/start.sh php-fpm
    run ./bin/status.sh
    [ $(echo $output | grep php-fpm | grep "5173->5173" | wc -l) -eq 1 ]
    sed -i "s|^\(VITE_PORT=\s*\).*$|\15000|" .env
    ./bin/start.sh php-fpm
    run ./bin/status.sh
    [ $(echo $output | grep php-fpm | grep "5000->5173" | wc -l) -eq 1 ]
}

@test $(get_description "check if cron-scripts are copied") {
    echo "hello world 12345" > ./contexts/php-fpm/system-cron/test_cron
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    run docker exec -t --user=root "${COMPOSE_PROJECT_NAME}_php-fpm" cat /etc/cron.d/test_cron
    [ $status -eq 0 ]
    [ $(echo $output | grep -i "hello world 12345" | wc -l) -eq 1 ]
}

@test $(get_description "check if memory limit and max execution time is set correctly") {
    sed -i "s|^\(PHP_EXT_PDO_MYSQL=\s*\).*$|\1false|" .env
    sed -i "s|^\(PHP_MEMORY_LIMIT=\s*\).*$|\1167|" .env
    sed -i "s|^\(PHP_UPLOAD_MAX_FILESIZE=\s*\).*$|\1168|" .env
    sed -i "s|^\(PHP_MAX_EXECUTION_TIME=\s*\).*$|\1123|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" php -r "echo ini_get('memory_limit');"
    [ $(echo $output | grep -i "167M" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" php -r "echo ini_get('upload_max_filesize');"
    [ $(echo $output | grep -i "168M" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" php -r "echo ini_get('post_max_size');"
    [ $(echo $output | grep -i "168M" | wc -l) -eq 1 ]
    docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c "echo '<?php echo \"Max execution time: \" . ini_get(\"max_execution_time\") . \"\n\";' > maxExecutionTime.php"
    docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" sudo apt-get install -y libfcgi0ldbl
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c "SCRIPT_FILENAME=/var/www/maxExecutionTime.php REQUEST_METHOD=GET cgi-fcgi -bind -connect 127.0.0.1:9000"
    [ $(echo $output | grep -i "Max execution time: 123" | wc -l) -eq 1 ]
}

@test $(get_description "check xdebug and pcov extension") {
    ./bin/start.sh php-fpm
    check_extension 'xdebug'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 0 ]
    check_extension 'pcov'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 0 ]
    sed -i "s|^\(PHP_EXT_XDEBUG=\s*\).*$|\1true|" .env
    sed -i "s|^\(PHP_EXT_PCOV=\s*\).*$|\1true|" .env
    sed -i "s|^\(PHP_EXT_PDO_MYSQL=\s*\).*$|\1false|" .env
    ./bin/stop.sh php-fpm
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'xdebug'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
    check_extension 'pcov'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check if the latest stable composer V1 version is installed") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    sed -i "s|^\(COMPOSER_MAJOR_VERSION=\s*\).*$|\11|" .env
    sed -i "s|^\(PHP_EXT_PDO_MYSQL=\s*\).*$|\1false|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" composer --version
    COMPOSER_VERSION_INSTALLED=$(echo $output | awk 'match($0,/([0-9]+\.[0-9]+\.[0-9]+)/) {print substr($0,RSTART,RLENGTH)}')
    COMPOSER_TAGS=$(docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" git ls-remote --tags https://github.com/composer/composer.git)
    COMPOSER_LAST_STABLE_VERSION=$(echo "$COMPOSER_TAGS" | awk -F'/' '/1\.[0-9]+\.[0-9]+[\r]$/ { print $3}' | sort -t "." -k1,1n -k2,2n -k3,3n | tail -1 | tr -d '\r')
    [ "$COMPOSER_VERSION_INSTALLED" = "$COMPOSER_LAST_STABLE_VERSION" ]
}

@test $(get_description "check if the latest stable composer V2 version is installed") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    sed -i "s|^\(COMPOSER_MAJOR_VERSION=\s*\).*$|\12|" .env
    sed -i "s|^\(PHP_EXT_PDO_MYSQL=\s*\).*$|\1false|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" composer --version
    COMPOSER_VERSION_INSTALLED=$(echo $output | awk 'match($0,/([0-9]+\.[0-9]+\.[0-9]+)/) {print substr($0,RSTART,RLENGTH)}')
    COMPOSER_TAGS=$(docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" git ls-remote --tags https://github.com/composer/composer.git)
    COMPOSER_LAST_STABLE_VERSION=$(echo "$COMPOSER_TAGS" | awk -F'/' '/2\.[0-9]+\.[0-9]+[\r]$/ { print $3}' | sort -t "." -k1,1n -k2,2n -k3,3n | tail -1 | tr -d '\r')
    [ "$COMPOSER_VERSION_INSTALLED" = "$COMPOSER_LAST_STABLE_VERSION" ]
}

@test $(get_description "check node version and yarn installed") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    sed -i "s|^\(NPM=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'node -v'
    echo $output
    [ $(echo $output | grep -i "v20." | wc -l) -eq 1 ]
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'yarn --version'
    echo $output
    [ $(echo $output | grep -i "1." | wc -l) -eq 1 ]
    sed -i "s|^\(NODE_VERSION=\s*\).*$|\123|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/restart.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'node -v'
    [ $(echo $output | grep -i "v23." | wc -l) -eq 1 ]
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'yarn --version'
    echo $output
    [ $(echo $output | grep -i "1." | wc -l) -eq 1 ]
}

@test $(get_description "check if the composer memory limit is set correctly") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    ./bin/start.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'echo $COMPOSER_MEMORY_LIMIT'
    echo $output
    [ $(echo $output | grep -i "6G" | wc -l) -eq 1 ]
    sed -i "s|^\(COMPOSER_MEMORY_LIMIT=\s*\).*$|\18G|" .env
    ./bin/restart.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm" bash -c 'echo $COMPOSER_MEMORY_LIMIT'
    [ $(echo $output | grep -i "8G" | wc -l) -eq 1 ]
}

@test $(get_description "check if sendmail for mailhog correctly installed and configured") {
    sed -i "s|^\(MAILHOG_SENDMAIL=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_package_is_installed "msmtp"
    [ $status -eq 0 ]
    check_package_is_installed "msmtp-mta"
    [ $status -eq 0 ]
    check_package_is_installed "mailutils"
    [ $status -eq 0 ]
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    MAILHOG_SMTP_PORT=$(grep -r MAILHOG_SMTP_PORT .env | cut -d= -f2)
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" php -r "echo ini_get('sendmail_path');"
    [ $(echo $output | grep -i "/usr/sbin/sendmail -S mailhog:${MAILHOG_SMTP_PORT}" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm" cat /etc/msmtprc
    [ $(echo $output | grep -i "port ${MAILHOG_SMTP_PORT}" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "host mailhog" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "from mailhog@example.com" | wc -l) -eq 1 ]
}

@test $(get_description "check mysqli extension") {
    sed -i "s|^\(PHP_EXT_MYSQLI=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'mysqli'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check mbstring extension") {
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'mbstring'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check pdo mysql extension") {
    sed -i "s|^\(PHP_EXT_PDO_MYSQL=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'pdo_mysql'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check ms sql extension") {
    sed -i "s|^\(PHP_EXT_MSSQL=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'sqlsrv'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
    check_extension 'pdo_sqlsrv'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check zip extension") {
    sed -i "s|^\(PHP_EXT_ZIP=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'zip'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check ldap extension") {
    sed -i "s|^\(PHP_EXT_LDAP=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'ldap'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check sockets extension") {
    sed -i "s|^\(PHP_EXT_SOCKETS=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'sockets'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check pcntl extension") {
    sed -i "s|^\(PHP_EXT_PCNTL=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'pcntl'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check decimal extension") {
    sed -i "s|^\(PHP_EXT_DECIMAL=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'decimal'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check redis extension") {
    sed -i "s|^\(DEFAULT_START_REDIS=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    check_extension 'redis'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check if composer gitlab domains contains gitlab.com by default") {
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    run ./bin/exec.sh composer global config gitlab-domains
    [ $(echo $output | grep -F '["gitlab.com"]' | wc -l) -eq 1 ]
    run ./bin/exec.sh sudo composer global config gitlab-domains
    [ $(echo $output | grep -F '["gitlab.com"]' | wc -l) -eq 1 ]
}

@test $(get_description "check if the additional gitlab domains were set correctly") {
    sed -i "s|^\(COMPOSER_GITLAB_DOMAINS=\s*\).*$|\1custom-gitlab.example.com,test-gitlab.example.org|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    run ./bin/exec.sh composer global config gitlab-domains
    [ $(echo $output | grep -F '["gitlab.com","custom-gitlab.example.com","test-gitlab.example.org"]' | wc -l) -eq 1 ]
    run ./bin/exec.sh sudo composer global config gitlab-domains
    [ $(echo $output | grep -F '["gitlab.com","custom-gitlab.example.com","test-gitlab.example.org"]' | wc -l) -eq 1 ]
}

@test $(get_description "check google chrome") {
    ./bin/start.sh php-fpm
    run ./bin/exec.sh google-chrome --version
    [ $status -ne 0 ]
    sed -i "s|^\(CYPRESS_DEPENDENCIES=\s*\).*$|\1true|" .env
    ./bin/subscripts/docker-compose.sh build php-fpm
    ./bin/start.sh php-fpm
    run ./bin/exec.sh google-chrome --version
    [ $status -eq 0 ]
    [ $(echo $output | grep -i "Google Chrome " | wc -l) -eq 1 ]
}

