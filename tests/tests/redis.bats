#!/usr/bin/env bats

load helpers

@test $(get_description "check if redis runs correctly and check start other redis version") {
   sed -i "s|^\(DEFAULT_START_NGINX=\s*\).*$|\1false|" .env
   sed -i "s|^\(DEFAULT_START_MYSQL=\s*\).*$|\1false|" .env
   sed -i "s|^\(DEFAULT_START_REDIS=\s*\).*$|\1true|" .env
   ./bin/subscripts/docker-compose.sh build php-fpm
   ./bin/start.sh
   run ./bin/exec.sh redis-cli -h redis info server
   [ $(echo $output | grep redis_version:7 | wc -l) -eq 1 ]
   sed -i "s|^\(REDIS_VERSION=\s*\).*$|\16|" .env
   ./bin/start.sh
   run ./bin/exec.sh redis-cli -h redis info server
   [ $(echo $output | grep redis_version:6 | wc -l) -eq 1 ]
}

@test $(get_description "check redis runs with other published port") {
   sed -i "s|^\(REDIS_PORT=\s*\).*$|\16000|" .env
   ./bin/start.sh redis
   run ./bin/status.sh
   [ $(echo $output | grep redis | grep "6000->6379" | wc -l) -eq 1 ]
}

@test $(get_description "check phpcacheadmin runs correctly") {
   ./bin/start.sh phpcacheadmin
   run ./bin/status.sh
   [ $(echo $output | grep redis | grep "6379->6379" | wc -l) -eq 1 ]
   [ $(echo $output | grep phpcacheadmin | grep "8090->80" | wc -l) -eq 1 ]
}

@test $(get_description "check phpcacheadmin runs with other published port") {
   sed -i "s|^\(PHPCACHEADMIN_PORT=\s*\).*$|\16000|" .env
   ./bin/start.sh phpcacheadmin
   run ./bin/status.sh
   [ $(echo $output | grep phpcacheadmin | grep "6000->80" | wc -l) -eq 1 ]
}
