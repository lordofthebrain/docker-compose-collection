#!/usr/bin/env bats

load helpers

run_script () {
    run bin/build.sh nginx apache
    [ $status -eq 0 ]
    run bin/start.sh nginx apache
    [ $status -eq 0 ]
}

@test $(get_description "paths") {
    sed -i "s|^\(DOCUMENT_ROOT=\s*\).*$|\1/var/123/456|" .env
    sed -i "s|^\(NGINX_PORT=\s*\).*$|\110021|" .env
    sed -i "s|^\(APACHE_PORT=\s*\).*$|\110022|" .env

    run_script

    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)

    run docker exec -t "${COMPOSE_PROJECT_NAME}_nginx" cat /etc/nginx/conf.d/default.conf
    [ $status -eq 0 ]
    [ $(echo $output | grep "/var/123/456" | wc -l) -eq 1 ]

    run docker exec -t "${COMPOSE_PROJECT_NAME}_apache" cat /usr/local/apache2/conf/httpd.conf
    [ $status -eq 0 ]
    [ $(echo $output | grep "/var/123/456" | wc -l) -eq 1 ]
}