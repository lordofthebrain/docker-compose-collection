#!/usr/bin/env bats

load helpers

run_script () {
    ./bin/start.sh php-fpm
    run bin/bash.sh -c pwd
}

run_script_with_parameters () {
    ./bin/start.sh php-fpm
    run bin/bash.sh -c "id root"
}

run_cli_script () {
    ./bin/start.sh php-fpm
    run bin/dcc bash -c pwd
}

@test $(get_description "check bash is working") {
    run_script
    [ $status -eq 0 ]
    [ $(echo $output | grep "/var/www" | wc -l) -eq 1 ]
}

@test $(get_description "check bash command with parameters is working") {
    run_script_with_parameters
    [ $status -eq 0 ]
    [ $(echo $output | grep "uid=0" | wc -l) -eq 1 ]
}

@test $(get_description "check bash is working via cli") {
    run_cli_script
    [ $status -eq 0 ]
    [ $(echo $output | grep "/var/www" | wc -l) -eq 1 ]
}

@test $(get_description "check if script runs when .env is missing") {
    check_env_test
    [ $(echo $output | grep "/var/www" | wc -l) -eq 1 ]
}

