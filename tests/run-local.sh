#!/bin/bash

set -eu

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
CONTAINER_NAME=dcc-dind
DOCKER_TAG=25-dind


if [ $(docker ps -a | grep ${CONTAINER_NAME} | wc -l) -ne 0 ]; then
	docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}
fi

docker run --privileged -d --name ${CONTAINER_NAME} -v ${SCRIPT_DIR}/..:/${CONTAINER_NAME} docker:${DOCKER_TAG}
docker exec --workdir=/${CONTAINER_NAME} -t ${CONTAINER_NAME} ./tests/prepare-run-tests.sh "${@}"
