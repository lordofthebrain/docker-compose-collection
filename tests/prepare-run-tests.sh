#!/bin/sh

set -eu

USER=pipeline_user

apk update && apk add --no-cache bash curl tzdata
id ${USER} &> /dev/null || (adduser ${USER} --gecos "" --disabled-password && (getent group docker &>/dev/null && addgroup ${USER} docker || true))
mkdir -p app_dir
chown ${USER} -R app_dir
echo '###########################'
echo '#####TESTS#################'
echo '###########################'
su -c "./tests/run.sh $*" ${USER}

