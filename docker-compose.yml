services:
    php-fpm:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/php-fpm:${PHP_VERSION:-8.3}'
        restart: '${RESTART_POLICY:-no}'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_php-fpm'
        build:
            context: ./contexts/php-fpm
            args:
                - PHP_VERSION=${PHP_VERSION:-8.3}
                - PHP_UID=${UID:-1000}
                - TIMEZONE=${TIMEZONE:-Europe/Berlin}
                - PHP_MEMORY_LIMIT=${PHP_MEMORY_LIMIT:-512}
                - PHP_UPLOAD_MAX_FILESIZE=${PHP_UPLOAD_MAX_FILESIZE:-100}
                - PHP_EXT_PDO_MYSQL=${PHP_EXT_PDO_MYSQL:-true}
                - PHP_EXT_ZIP=${PHP_EXT_ZIP:-false}
                - PHP_EXT_GD=${PHP_EXT_GD:-false}
                - PHP_EXT_EXIF=${PHP_EXT_EXIF:-false}
                - PHP_EXT_BCMATH=${PHP_EXT_BCMATH:-false}
                - PHP_EXT_INTL=${PHP_EXT_INTL:-false}
                - PHP_EXT_MSSQL=${PHP_EXT_MSSQL:-false}
                - PHP_EXT_XDEBUG=${PHP_EXT_XDEBUG:-false}
                - PHP_EXT_PCOV=${PHP_EXT_PCOV:-false}
                - NPM=${NPM:-false}
                - NODE_VERSION=${NODE_VERSION:-18}
                - CYPRESS_DEPENDENCIES=${CYPRESS_DEPENDENCIES:-false}
                - MAILHOG_SENDMAIL=${MAILHOG_SENDMAIL:-false}
                - MAILHOG_SMTP_PORT=${MAILHOG_SMTP_PORT:-1025}
                - PHP_EXT_MYSQLI=${PHP_EXT_MYSQLI:-false}
                - PHP_MAX_INPUT_VARS=${PHP_MAX_INPUT_VARS:-1000}
                - PHP_MAX_EXECUTION_TIME=${PHP_MAX_EXECUTION_TIME:-30}
                - PHP_EXT_LDAP=${PHP_EXT_LDAP:-false}
                - PHP_EXT_SOCKETS=${PHP_EXT_SOCKETS:-false}
                - PHP_EXT_PCNTL=${PHP_EXT_PCNTL:-false}
                - PHP_EXT_DECIMAL=${PHP_EXT_DECIMAL:-false}
                - COMPOSER_GITLAB_DOMAINS=${COMPOSER_GITLAB_DOMAINS:-}
                - COMPOSER_MAJOR_VERSION=${COMPOSER_MAJOR_VERSION:-2}
                - REDIS=${DEFAULT_START_REDIS:-false}
        environment:
            COMPOSER_MEMORY_LIMIT: '${COMPOSER_MEMORY_LIMIT:-6G}'
        user: www-data
        working_dir: /var/www
        extra_hosts:
            - host.docker.internal:host-gateway
        volumes:
            - '${APP_DIR:-../}:/var/www'
        networks:
            - backend
    nginx:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/nginx'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_nginx'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/nginx
            args:
                - 'NGINX_UID=${UID:-1000}'
                - 'TIMEZONE=${TIMEZONE:-Europe/Berlin}'
                - 'DOCUMENT_ROOT=${DOCUMENT_ROOT:-/var/www/public}'
        working_dir: /var/www
        volumes:
            - '${APP_DIR:-../}:/var/www'
        depends_on:
            - php-fpm
        networks:
            - backend
    apache:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/apache'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_apache'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/apache
            args:
                - 'APACHE_UID=${UID:-1000}'
                - 'TIMEZONE=${TIMEZONE:-Europe/Berlin}'
                - 'DOCUMENT_ROOT=${DOCUMENT_ROOT:-/var/www/public}'
        working_dir: /var/www
        volumes:
            - '${APP_DIR:-../}:/var/www'
        depends_on:
            - php-fpm
        networks:
            - backend
    mysql:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/mysql:${MYSQL_VERSION:-8}'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_mysql'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/mysql
            args:
                - 'MYSQL_VERSION=${MYSQL_VERSION:-8}'
                - 'MYSQL_UID=${UID:-1000}'
                - 'TIMEZONE=${TIMEZONE:-Europe/Berlin}'
        volumes:
            - '${DATA_DIR:-../storage/docker-compose-data}/mysql-${MYSQL_VERSION:-8}:/var/lib/mysql'
            - './contexts/mysql/mysql-initdb.d:/docker-entrypoint-initdb.d'
        environment:
            - 'MYSQL_USER=${MYSQL_USER:-homestead}'
            - 'MYSQL_PASSWORD=${MYSQL_PASSWORD:-secret}'
            - 'MYSQL_DATABASE=${MYSQL_DATABASE:-}'
            - 'MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD:-root}'
        networks:
            - backend
    phpmyadmin:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/phpmyadmin'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_phpmyadmin'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/phpmyadmin
        environment:
            - 'PMA_HOST=mysql'
            - 'PMA_USER=root'
            - 'PMA_PASSWORD=${MYSQL_ROOT_PASSWORD:-root}'
        depends_on:
            - mysql
        networks:
            - backend
    redis:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/redis:${REDIS_VERSION:-7}-alpine'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_redis'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/redis
            args:
                - 'REDIS_VERSION=${REDIS_VERSION:-7}'
                - 'REDIS_UID=${UID:-1000}'
                - 'TIMEZONE=${TIMEZONE:-Europe/Berlin}'
        volumes:
            - '${DATA_DIR:-../storage/docker-compose-data}/redis-${REDIS_VERSION:-7}:/data'
        healthcheck:
            test: [ "CMD", "redis-cli", "ping" ]
        networks:
            - backend
    phpcacheadmin:
        image: robinn/phpcacheadmin
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_phpcacheadmin'
        restart: '${RESTART_POLICY:-no}'
        environment:
            - 'PCA_REDIS_0_HOST=redis'
            - 'PCA_REDIS_0_PORT=6379'
        depends_on:
            - redis
        networks:
            - backend
    selenium-chrome:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/selenium-chrome'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_selenium-chrome'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/selenium-chrome
        networks:
            - backend
    selenium-firefox:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/selenium-firefox'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_selenium-firefox'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/selenium-firefox
        networks:
            - backend
    mailhog:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/mailhog'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_mailhog'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/mailhog
        networks:
            - backend
    httpbin:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/httpbin'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_httpbin'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/httpbin
        networks:
            - backend
    test-picture-provider:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/test-picture-provider'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_test-picture-provider'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/test-picture-provider
        volumes:
            - '${DATA_DIR:-../storage/docker-compose-data}/test-picture-provider:/app/pictures'
        networks:
            - backend
    chrome:
        image: '${COMPOSE_PROJECT_NAME:-my_fancy_app}/chrome'
        container_name: '${COMPOSE_PROJECT_NAME:-my_fancy_app}_chrome'
        restart: '${RESTART_POLICY:-no}'
        build:
            context: ./contexts/chrome
        networks:
            - backend

networks:
    backend:
        driver: bridge

